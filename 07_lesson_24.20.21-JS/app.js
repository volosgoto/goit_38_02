class User {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    //     getInfo(){
    //         console.log(`${this.name} ${this.age}`);
    //    }

    race = "human";

    // getInfo = () => {}

    getInfo = () => {
        // console.log(this);
        console.log(`${this.name} ${this.age}`);
    };
}

let vova = new User("Vova", 25);
let sara = new User("Sara", 19);
let bob = new User("Sara", 30);
let mike = new User("Mike", 45);

console.log(vova.race);
console.log(mike.race);

// vova.getInfo();
// sara.getInfo();
// bob.getInfo();
// mike.getInfo();

// =====================================
// class Calculator {
//     static PI = 3.14;

//     static sum = (a, b) => {
//         return a + b;
//     };

//     static circleSQRT = (radius) => {
//         console.log(this);
//         return this.PI * radius ** radius;
//     };
// }

// let result = Calculator.PI;
// result = Calculator.sum(10, 21);
// result = Calculator.circleSQRT(5);

// console.log(result);
