const cart = [
    {
        name: 'Apple',
        price: 1000,
        quantity: 2,
    },
    {
        name: 'Samsung',
        price: 700,
        quantity: 5,
    },
    {
        name: 'Huawei',
        price: 500,
        quantity: 5,
    },
    {
        name: 'Xiaomi',
        price: 200,
        quantity: 11,
    },
];

// const getTotalPrice = cart => {
//   let totalPrice = 0;
//   for (const cartItem of cart) {
//     totalPrice += cartItem.price * cartItem.quantity;
//   }
//   return totalPrice;
// };

// console.log(getTotalPrice(cart));

// const TotalPrice = cart.reduce((totalPrice, cartItem) => {
//   return totalPrice + cartItem.price * cartItem.quantity;
// }, 0);

// console.log(TotalPrice);

// const bmw = {
//     title: 'Bmw',
//     model: 'X5',
//     year: '2005',
// };
// const toyota = {
//     title: 'Toyota',
//     model: 'Camry',
//     year: '1995',
// };

// const carInfo = {
//   showTotalInfo() {
//     console.log(
//       `Марка: ${this.title}, Модель: ${this.model}, Год Выпуска: ${this.year}`,
//     );
//   },
//   setTitle(newTitle) {
//     return (this.title = newTitle);
//   },
//   addNewPropery(propName, propValue) {
//     // for (let car of this) {
//     //   car[propName] = propValue;
//     // }
//     console.log(this);
//     this[propName] = propValue;
//   },
// };

// carInfo.showTotalInfo.call(bmw);
// carInfo.showTotalInfo.call(toyota);

// const showBmwInfo = carInfo.showTotalInfo.bind(bmw);
// const showToyotaInfo = carInfo.showTotalInfo.bind(toyota);

// showBmwInfo();
// showBmwInfo();

// Карирование
// carInfo.showTotalInfo.bind(toyota)();

// carInfo.setTitle.call(bmw, 'BMWm');
// console.log(bmw);

// carInfo.setTitle.bind(bmw, 'Bavaria')();
// console.log(bmw);

// carInfo.addNewPropery.bind(bmw, 'color', 'red')();
// console.log(bmw);

// const autoSalon = [
//     {
//         title: 'Bmw',
//         model: 'X5',
//         year: '2005',
//     },
//     {
//         title: 'Toyota',
//         model: 'Camry',
//         year: '1995',
//     },
// ];

// const carInfo = {
//     showTotalInfo() {
//         console.log(
//             `Марка: ${this.title}, Модель: ${this.model}, Год Выпуска: ${this.year}`,
//         );
//     },
//     setTitle(newTitle) {
//         return (this.title = newTitle);
//     },
//     addNewPropery(propName, propValue) {
//         for (let car of this) {
//             car[propName] = propValue;
//         }
//     },
// };

// carInfo.addNewPropery.call(autoSalon, 'color', 'black');
// console.log(autoSalon);

//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////
//////

let people = [
    { id: 1, name: 'Vova', age: 22, budget: 4000, isComlete: true },
    { id: 2, name: 'Sara', age: 30, budget: 3400, isComlete: false },
    { id: 3, name: 'Mike', age: 45, budget: 1500, isComlete: true },
    { id: 4, name: 'Bob', age: 18, budget: 400, isComlete: false },
];

const totalSalary = people.map(person => {
    return {
        ...person,
        isComlete: (person.isComlete = true),
        budget: person.budget + person.budget * 0.2,
        adress: 'Kyiv',
        hobbies: 'Football',
    };
});

console.log(totalSalary);
