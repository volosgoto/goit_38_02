let people = [
    { id: 1, name: 'Vova', age: 22, budget: 4000, isComlete: true },
    { id: 2, name: 'Sara', age: 30, budget: 3400, isComlete: false },
    { id: 3, name: 'Mike', age: 45, budget: 1500, isComlete: true },
    { id: 4, name: 'Bob', age: 18, budget: 400, isComlete: false },
];

// const peopleInfo = people.forEach((person, index, array) => {
//     // console.log(person);
//     // console.log(
//     //     `name: ${person.name}, age: ${person.age}, budget: ${person.budget}`,
//     // );
//     const { name, age, budget } = person;
//     // console.log(`name: ${name}, age: ${age}, budget: ${budget}`);
//     return `name1: ${name}, age1: ${age}, budget1: ${budget}`;
// });
// console.log(peopleInfo);

const peopleInfo = people.map((person, index, array) => {
    // console.log(person);
    // console.log(
    //     `name: ${person.name}, age: ${person.age}, budget: ${person.budget}`,
    // );
    const { name, age, budget } = person;
    return { name, age, budget };
});
// console.log(peopleInfo);
// console.log(people);

// (element, index, array)=> {}
// (accomulator,element, index, array)=> {}

// [].forEach(
//     (element, index, array)=> {})
// [].map()
// [].sort()
// [].filter()
// [].find()
// [].every()
// [].some()
// [].reduce(callback, accomulator)

// [].push
// [].slice
// [].splice

//
//
//
//
//
//
//
//
//
//

class Shop {
    #items;
    constructor(items) {
        this.#items = items;
    }

    showItems() {
        this.#items.forEach(({ id, name, price, qty }) => {
            console.log(
                `id - ${id}, name - ${name}, price - ${price}, qty - ${qty} `,
            );
        });
    }

    addItem(name, price, qty, category) {
        let newObject = {
            id: this.#generateItemId(),
            name,
            price,
            qty,
            category,
        };
        this.#items = [...this.#items, newObject];
    }

    updateItem(itemName, nameToUpdate) {
        let objectToUpdate = this.#items.find(item => item.name === itemName);
        objectToUpdate.name = nameToUpdate;
    }

    deleteItem(itemToDelete) {
        this.#items = this.#items.filter(item => item.name !== itemToDelete);
    }

    #generateItemId() {
        return Math.random().toString().slice(2) + 1;
    }
}

const items = [
    { id: 'id-1', name: 'apples', price: 55, qty: 500, category: 'fruits' },
    { id: 'id-2', name: 'potato', price: 23, qty: 875, category: 'fruits' },
    { id: 'id-3', name: 'bananes', price: 50, qty: 400, category: 'fruits' },
    { id: 'id-4', name: 'tomatoes', price: 35, qty: 650, category: 'fruits' },
];

const shop = new Shop(items);

// shop.addItem('lemon', 50, 200, 'fruits');
// shop.showItems();
// shop.updateItem('apples', 'lime');
shop.deleteItem('bananes');
shop.showItems();

const citiesOfUkraine = ['Odessa', 'Dnepr', 'Kyiv'];
const citiesOfEurope = ['Oslo', 'Milan', 'Madrid'];
const allCities = [...citiesOfUkraine, ...citiesOfEurope, 'NY'];
// console.log(allCities);

let people = [
    { id: 1, name: 'Vova', age: 22, budget: 4000, isComlete: true },
    { id: 2, name: 'Sara', age: 30, budget: 3400, isComlete: false },
    { id: 3, name: 'Mike', age: 45, budget: 1500, isComlete: true },
    { id: 4, name: 'Bob', age: 18, budget: 400, isComlete: false },
];
