class Shop {
    #items;
    constructor(items) {
        this.#items = items;
    }

    showItems() {
        for (const item of this.#items) {
            const { id, name, price, qty } = item;
            console.log(
                `id - ${id}, name - ${name}, price - ${price}, qty - ${qty} `,
            );
        }
    }

    addItem(name, price, qty, category) {
        let newObject = {
            id: this.#generateItemId(),
            name,
            price,
            qty,
            category,
        };
        this.#items.push(newObject);
    }

    updateItem(itemName, nameToUpdate) {
        for (const item of this.#items) {
            if (item.name === itemName) {
                item.name = nameToUpdate;
            }
        }
    }

    deleteItem(itemToDelete) {
        for (const item of this.#items) {
            if (item.name === itemToDelete) {
                let index = this.#items.indexOf(item);
                this.#items.splice(index, 1);
            }
        }
    }

    #generateItemId() {
        return Math.random().toString().slice(2) + 1;
    }
}

const items = [
    { id: 'id-1', name: 'apples', price: 55, qty: 500, category: 'fruits' },
    { id: 'id-2', name: 'potato', price: 23, qty: 875, category: 'fruits' },
    { id: 'id-3', name: 'bananes', price: 50, qty: 400, category: 'fruits' },
    { id: 'id-4', name: 'tomatoes', price: 35, qty: 650, category: 'fruits' },
];

const shop = new Shop(items);

shop.addItem('lemon', 50, 200, 'fruits');
// shop.showItems();
shop.updateItem('lemon', 'lime');
shop.deleteItem('bananes');
shop.showItems();
