let a = 'a';

// 27
console.log(String.fromCharCode(97));
console.log(a.charCodeAt());

// let alf = ['a', 'b', 'c']

// const arr = [1, 2, 45, 34, 87, 43,];

// const createNewArray = function (arr) {

//     let newArr = [];

//     for (let item of arr){
//         newArr.push(item);
//     };

//     return newArr;

// }

// console.log(createNewArray(arr));
// console.log(createNewArray(arr)===arr);

const celarys = [10000, 11000, 12000, 15000, 13500];

const addBonus = function (selarys, bonus) {
    let bonusCelarys = [];

    for (let item of selarys) {
        bonusCelarys.push(item + (item * bonus) / 100);
    }

    return bonusCelarys;
};

console.log(addBonus(celarys, 10));

const getTotalCelerys = function (celarys) {
    let total = 0;

    for (let item of celarys) {
        total += item;
    }

    return total;
};

console.log(getTotalCelerys(addBonus(celarys, 10)));
// ===================================
const vova = {
    name: 'Vova',
    salary: '',
    salaryTnasfer: false,
};

const sara = {
    name: 'Sara',
    salary: '',
    salaryTnasfer: false,
};

const bob = {
    name: 'Bob',
    salary: '',
    salaryTnasfer: false,
};

const employeeInfo = {
    showInfo() {
        console.log(
            `Name: ${this.name}, Salary: ${this.salary}, Salary status: ${this.salaryTnasfer} `,
        );
    },

    updateSalaryTranser(newStatus) {
        this.salaryTnasfer = newStatus;
    },

    updateSalary(newSalary) {
        this.salary = newSalary;
    },
};

employeeInfo.updateSalary.apply(bob, [10000]);
employeeInfo.updateSalaryTranser.call(bob, true);
employeeInfo.showInfo.bind(bob)();

// ==================================================================

const BMW = {
    model: 'X5',
    year: 2021,
};

const audi = {
    model: 'A6',
    year: 2005,
};

const mercedes = {
    model: 'Vito',
    year: 2006,
};

const germanCars = [BMW, audi, mercedes];

const carInfo = {
    showInfo() {
        let keys = Object.keys(this);
        let value = Object.values(this);
        for (let key in keys) {
            console.log(keys[key], ':', value[key]);
        }
    },

    addProperty(propertyKey, propertyValue) {
        for (let car of this) {
            car[propertyKey] = propertyValue;
        }
    },
};

carInfo.showInfo.call(mercedes);

carInfo.addProperty.call(germanCars, 'color', 'black');

console.log(germanCars);

// ==============================================
const calculator = function (a, b, callback) {
    return callback(a, b);
};

// 5+10 = 15;

const sum = (a, b) => a + b;
const mult = (a, b) => a * b;
const sub = (a, b) => a - b;
const div = (a, b) => a / b;

console.log(calculator(5, 10, sum));
console.log(calculator(5, 10, mult));
console.log(calculator(5, 10, sub));
console.log(calculator(5, 10, div));

// ==================================
const builder = new StringBuilder('.');
console.log(builder.getValue()); // '.' builder.padStart('^');
console.log(builder.getValue()); // '^.' builder.padEnd('^');
console.log(builder.getValue()); // '^.^' builder.padBoth('=');
console.log(builder.getValue()); // '=^.^='

const builder = new StringBuilder('.');
builder.padStart('^').builder.padEnd('^').builder.padBoth('=');
