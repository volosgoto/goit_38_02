class StringBuilder {
    constructor(baseValue) {
        this.value = baseValue;
    }

    getValue = function () {
        return this.value;
    };

    padEnd = function (str) {
        this.value += str;
        return this;
    };

    padStart = function (str) {
        this.value = str + this.value;
        return this;
    };

    padBoth = function (str) {
        this.padStart(str);
        this.padEnd(str);
        return this;
    };
}

const builder = new StringBuilder('.');
//   console.log(builder.getValue()); // '.'
//   builder.padStart('^');
//   console.log(builder.getValue()); // '^.'
//   builder.padEnd('^');
//   console.log(builder.getValue()); // '^.^'
//   builder.padBoth('=');
//   console.log(builder.getValue()); // '=^.^='

builder.padStart('^').padEnd('^').padBoth('=');

console.log(builder.getValue()); // '.'

const str = 'abc';

const result = str.split('').reverse().join('');
console.log(result);
