function sum(a, b) {
    console.log(a + b);
    console.log('sum', this);
}

// sum(5, 10);
// this.sum(5, 10);
// window.sum(5, 10);

// let vova = {
//     name: 'Vova',
//     getInfo() {
//         console.log(this);
//         sum(5, 10);
//     },
// };

// vova.getInfo();

console.log('===========================================');

// let sara = {
//     name: 'Sara',
//     getInfo() {
//         console.log(this);
//         sum(5, 10); // Window
//         // sum.bind(this, 5, 10); // Sara
//     },
// };

// sara.getInfo();
// =====================================
class User {
    constructor(name) {
        this.name = name;
    }
    getInfo() {
        console.log(this);
    }

    // 1. constructor()
    // 2. {}
    // 3. привязка this
    // 4. возвращает обьект с привязкой this
}

let bob = new User('Bob');
bob.getInfo();

let mike = new User('Mike');
mike.getInfo();

// =========================================
// function User(name) {
//     this.name = name;
//     this.getInfo = function () {
//         console.log(this);
//     };

//     // 1. {}
//     // 2. привязка this
//     // 3. возвращает обьект с привязкой this
// }

// // console.log(User);
// let bob = new User('Bob');
// bob.getInfo();

// let mike = new User('Mike');
// mike.getInfo();
