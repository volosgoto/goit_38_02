// String;
// Number;
// Boolean; // true / false
// undefined;
// null;
// BigInt;
// Symbol;

// Object;

// ==
// ===
// <
// >
// <=
// >=
// !
// !!
// &&
// ||
// ~

// String;
// Number;
// Boolean;

// let result = 'vova' === 'vova';

// console.log(typeof result);

// if (!isNaN('16' * 4)) {
//     console.log('Boolean true');
// } else {
//     console.log('Когда false');
// }

// Falsy values
// '' - пустая строка
// 0
// false
// null
// undefined
// NaN

// String; +
// Number; +
// Boolean;

// -
// /
// *
// **
// %

// let result = '16' * 4; // 64
// result = 4 * '16'; // 64
// result = 4 * '16px'; // 64
// result = 4 - '16px'; // 64
// result = '16px' - 4; // 64
// console.log(typeof result);
// console.log(result);

// let result = isNaN(4 * '16'); // false
// let result = isNaN(4 * '16px'); // true
// console.log(result);
