// Simple

const refs = {
    form: document.querySelector(".js-feedback-form"),
    textArea: document.querySelector(".js-feedback-message"),
};

getFromLS();

function getFromLS() {
    const messageFromLS = localStorage.getItem("messageInput");
    if (messageFromLS) {
        refs.textArea.value = messageFromLS;
    }
}

function onTextAreaInput(evt) {
    const messageInput = evt.target.value;
    localStorage.setItem("messageInput", messageInput);
}

function onFormSubmit(evt) {
    evt.preventDefault();
    const messageFromLS = localStorage.getItem("messageInput");
    if (messageFromLS) {
        localStorage.removeItem("messageInput");
        evt.target.reset();
    }
}

refs.textArea.addEventListener("input", onTextAreaInput);
refs.form.addEventListener("submit", onFormSubmit);

// localStorage.getItem('key');
// localStorage.setItem('key', 'value');
// localStorage.clear();
// localStorage.removeItem('key');

// const userName = 'Vova';

// localStorage.setItem('user', userName);

// const userFromLS = localStorage.getItem('user');

// if (userFromLS) {
//   localStorage.removeItem('user');
// }
