// APP
// refs.form.submit(); //. принудительная отправка формы

const refs = {
    form: document.querySelector(".js-feedback-form"),
    textArea: document.querySelector(".js-feedback-message"),
};

getFromLS();

function saveToLocalStorage(key, value) {
    localStorage.setItem(key, value);
}

function onFormElementsInput(evt) {
    const messageInput = evt.target.value;
    const key = evt.target.name;

    // if (key !== "pass") {
    //     saveToLocalStorage(key, messageInput);
    // }

    if (evt.target.type !== "password") {
        saveToLocalStorage(key, messageInput);
    }
}

function getFromLS() {
    const keysArray = Object.keys(localStorage);

    keysArray.forEach((formElementName) => {
        if (formElementName) {
            refs.form.elements[formElementName].value =
                localStorage.getItem(formElementName);
        }
    });
}

function onFormSubmit(evt) {
    evt.preventDefault();
    const formData = new FormData(evt.target);

    const dataFromForm = {};

    formData.forEach((value, key) => {
        dataFromForm[key] = value;
    });

    evt.target.reset();
    localStorage.clear();
}

refs.form.addEventListener("submit", onFormSubmit);
refs.form.addEventListener("input", onFormElementsInput);
