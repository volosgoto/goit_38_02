// let vova = {
//     name: 'Vova',
//     age: 30,
//     status: 'Admin',
// };

// console.log(vova);

// let sara = {
//     ...vova,
//     name: 'Sara',
//     car: {
//         manufacturer: 'BMW',
//         model: 'X5',
//     },
//     hobby: 'Facebook',
//     adress: 'Lviv',
// };

// sara.hobby = 'instagram';
// sara.adress = 'Kyiv';
// console.log(typeof sara);
// console.log(sara);

// let citiesOfUkraine = [
//     'Kuiv',
//     'Odessa',
//     'Donetsk',
//     'Berlin',
//     'London',
//     'Paris',
//     'New York',
// ];
// console.log(typeof citiesOfUkraine);
// console.log(citiesOfUkraine);

// AA
// BH
// AN

// Destucturing
// {}
// let { name: userName, age: vozrast } = vova;
// let { model } = sara.car;

// console.log(userName, vozrast);
// []

// let [AA, BH, AH, DE] = citiesOfUkraine;
// let [, , , DE, ...rest] = citiesOfUkraine;
// console.log(rest);

// let a = 2;
// let b = 5;
// console.log((a = 4), (b = 10));
// ===============================================

// let countries = ['Usa', 'GB', 'China', 'Russia'];
// let vakcines = ['Phizer', 'AstraZeneca', 'Coronovac', 'Sputnik'];

// {
//     Usa: Phizer,
//     GB: AstraZeneca,
//     China: Coronovac
// }

// let vova = {
//     name: 'Vova',
//     age: 30,
//     status: 'Admin',
// };

// console.log(vova.name);
// console.log(vova.status);

// console.log(vova['name']);
// console.log(vova['age']);

let a = 100;
let b = 200;

// console.log(a);

// [, b] = [200, 100];

console.log(b);

// ========================================
// Linc and Value

// let a = 100;
// let b = 200;
// console.log(a);

// a = b;

// console.log(a);
// console.log(a === b);

// let a = [1, 2, 3, 4];
// let b = a;
// b.push(5);

// let c = [1, 2, 3, 4, 5];
// console.log(c);
// console.log(a === c);
// console.log(b === c);
